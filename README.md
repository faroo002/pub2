# ML opportunities for Genomic Prediction (GP)
**#Step-1: Setting up conda environment**
There are two ways to install required R packages for this analysis;
1. Import the conda environment with dependency issues resolved already. We have tested it on a Ubuntu machine, whereas, other Linux/Windows OS may get some issues during dependency resolution.

    conda env create -f pub2.yml

    conda activate h2o4gpu3

Note: The following packages are not available through conda repositories, so need to be installed from source;

	install.packages("BEDMatrix", dependencies = TRUE)

	setRepositories(ind = 1:2)

	devtools::install_github("samuelbfernandes/simplePHENOTYPES", build_vignettes = TRUE)

2. Install packages individually on R v3.6.1

**#Step-2: Simulations**

setwd('./code/simulations')

#_use_large_effect_ = {0,1}

#_h2_ = {0.1:0.7}

- GBLUP

    Rscript gblup_linear.R #Samples #SNPs _h2_ _use_large_effect_

    Rscript gblup_nonlinear.R #Samples #SNPs _h2_ _use_large_effect_
- RKHS

    Rscript rkhs_linear.R #Samples #SNPs _h2_ _use_large_effect_

    Rscript rkhs_nonlinear.R #Samples #SNPs _h2_ _use_large_effect_
- BayesA

    Rscript bayesa_linear.R #Samples #SNPs _h2_ _use_large_effect_

    Rscript bayesa_nonlinear.R #Samples #SNPs _h2_ _use_large_effect_
- BayesB

    Rscript bayesb_linear.R #Samples #SNPs _h2_ _use_large_effect_

    Rscript bayesb_nonlinear.R #Samples #SNPs _h2_ _use_large_effect_    
- RandomForest

    Rscript rf_linear.R #Samples #SNPs _h2_ _use_large_effect_

    Rscript rf_nonlinear.R #Samples #SNPs _h2_ _use_large_effect_
- XGBoost

    Rscript xgb_linear.R #Samples #SNPs _h2_ _use_large_effect_
    
    Rscript xgb_nonlinear.R #Samples #SNPs _h2_ _use_large_effect_
- Visualizations

    Rscript plots.R

**#Step-3: Population structure analaysis**

**3.1. Real Arabidopsis RegMap 1307 accessions with simulated phenotypes**

setwd('./code/population_structure/simulations')
- GBLUP

    Rscript gblup.R _h2_ _use_large_effect_

- RKHS

    Rscript rkhs.R _h2_ _use_large_effect_

- BayesA

    Rscript bayesa.R _h2_ _use_large_effect_

- BayesB

    Rscript bayesb.R _h2_ _use_large_effect_

- RandomForest

    Rscript rf.R _h2_ _use_large_effect_

- XGBoost

    Rscript xgb.R _h2_ _use_large_effect_
    
- Visualizations

    Rscript plots.R


**3.2. Real Arabidopsis RegMap 1307 accessions subset with real phenotypes (Baxter et al., 2010)**

setwd('./code/population_structure/real')
- GBLUP

    Rscript gblup.R

- RKHS

    Rscript rkhs.R

- BayesA

    Rscript bayesa.R

- BayesB

    Rscript bayesb.R

- RandomForest

    Rscript rf.R

- XGBoost

    Rscript xgb.R
    
- Visualizations

    Rscript plots.R

**#Step-4: SNP-QTL LD analaysis**

**4.1. Real Arabidopsis HapMap 360 core accessions with simulated phenotypes**

setwd('./code/snp_qtn_ld/simulations')
- GBLUP

    Rscript gblup.R _#SNPs_ _#QTNs_ _h2_ _min_r2_ _max_r2_

- RKHS

    Rscript rkhs.R _#SNPs_ _#QTNs_ _h2_ _min_r2_ _max_r2_

- BayesA

    Rscript bayesa.R _#SNPs_ _#QTNs_ _h2_ _min_r2_ _max_r2_

- BayesB

    Rscript bayesb.R _#SNPs_ _#QTNs_ _h2_ _min_r2_ _max_r2_

- RandomForest

    Rscript rf.R _#SNPs_ _#QTNs_ _h2_ _min_r2_ _max_r2_

- XGBoost

    Rscript xgb.R _#SNPs_ _#QTNs_ _h2_ _min_r2_ _max_r2_
    
- Visualizations

    Rscript plots.R


**4.2. Real Soybean dataset with real phenotypes (Xavier, Muir et al. 2016)**

setwd('./code/snp_qtn_ld/real')
- GBLUP

    Rscript gblup.R

- RKHS

    Rscript rkhs.R

- BayesA

    Rscript bayesa.R

- BayesB

    Rscript bayesb.R

- RandomForest

    Rscript rf.R

- XGBoost

    Rscript xgb.R
    
- Visualizations

    Rscript plots.R

**#Step-5: Analyzing wheat traits**

**5.1. Real Wheat dataset with real phenotypes (Norman et al. 2017)**

setwd('./code/wheat/')

#Here trait argument could be one of the following set: {"BM","GH","GL","GP","GR","GY","LL","LW","NDVI","PH","TKW","TW","Y"}

- GBLUP

    Rscript gblup.R  _Trait_ _#Training-Samples_ _#Training_repeats_ 

- RKHS

    Rscript rkhs.R _Trait_ _#Training-Samples_ _#Training_repeats_ 

- BayesA

    Rscript bayesa.R _Trait_ _#Training-Samples_ _#Training_repeats_ 

- BayesB

    Rscript bayesb.R _Trait_ _#Training-Samples_ _#Training_repeats_ 

- RandomForest

    Rscript rf_raw_using_covariates.R _Trait_ _#Training-Samples_ _#Training_repeats_ _m_ _nodesize_

    #Here mtry=round(round(sqrt(#SNPs+#Covariates))*m)
- XGBoost

    Rscript xgb_raw_using_covariates.R _Trait_ _#Training-Samples_ _#Training_repeats_ _max_depth_ _colsample_bytree_ _subsample_
    
- Visualizations

    Rscript plots.R
