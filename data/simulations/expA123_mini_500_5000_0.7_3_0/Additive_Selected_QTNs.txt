rep	snp	allele	chr	pos	cm	maf
1	SNP1967	A/a	1	1	NA	0.408
1	SNP4152	A/a	1	1	NA	0.437
1	SNP4909	A/a	1	1	NA	0.387
1	SNP3336	A/a	1	1	NA	0.396
1	SNP1354	A/a	1	1	NA	0.393
1	SNP758	A/a	1	1	NA	0.386
1	SNP1772	A/a	1	1	NA	0.365
1	SNP2444	A/a	1	1	NA	0.395
1	SNP1189	A/a	1	1	NA	0.384
1	SNP1578	A/a	1	1	NA	0.404
1	SNP1216	A/a	1	1	NA	0.395
1	SNP2191	A/a	1	1	NA	0.393
1	SNP3057	A/a	1	1	NA	0.393
1	SNP3488	A/a	1	1	NA	0.412
1	SNP4133	A/a	1	1	NA	0.409
1	SNP4536	A/a	1	1	NA	0.399
1	SNP3661	A/a	1	1	NA	0.417
1	SNP1535	A/a	1	1	NA	0.416
1	SNP3749	A/a	1	1	NA	0.395
1	SNP4223	A/a	1	1	NA	0.402
1	SNP1475	A/a	1	1	NA	0.384
1	SNP133	A/a	1	1	NA	0.409
1	SNP75	A/a	1	1	NA	0.42
1	SNP3789	A/a	1	1	NA	0.388
1	SNP1275	A/a	1	1	NA	0.39
1	SNP1022	A/a	1	1	NA	0.414
1	SNP1748	A/a	1	1	NA	0.417
1	SNP3593	A/a	1	1	NA	0.396
1	SNP3592	A/a	1	1	NA	0.358
1	SNP2728	A/a	1	1	NA	0.417
1	SNP4877	A/a	1	1	NA	0.412
1	SNP1154	A/a	1	1	NA	0.406
1	SNP1888	A/a	1	1	NA	0.411
1	SNP3470	A/a	1	1	NA	0.386
1	SNP3018	A/a	1	1	NA	0.421
1	SNP4188	A/a	1	1	NA	0.397
1	SNP3757	A/a	1	1	NA	0.392
1	SNP4790	A/a	1	1	NA	0.398
1	SNP2927	A/a	1	1	NA	0.414
1	SNP1147	A/a	1	1	NA	0.394
1	SNP2206	A/a	1	1	NA	0.391
1	SNP1	A/a	1	1	NA	0.407
1	SNP1428	A/a	1	1	NA	0.38
1	SNP532	A/a	1	1	NA	0.394
1	SNP1650	A/a	1	1	NA	0.391
1	SNP802	A/a	1	1	NA	0.382
1	SNP1177	A/a	1	1	NA	0.404
1	SNP4283	A/a	1	1	NA	0.4
1	SNP2566	A/a	1	1	NA	0.412
1	SNP2910	A/a	1	1	NA	0.397
1	SNP1733	A/a	1	1	NA	0.382
1	SNP1584	A/a	1	1	NA	0.406
1	SNP2695	A/a	1	1	NA	0.391
1	SNP1757	A/a	1	1	NA	0.401
1	SNP1150	A/a	1	1	NA	0.385
1	SNP1864	A/a	1	1	NA	0.387
1	SNP405	A/a	1	1	NA	0.417
1	SNP1335	A/a	1	1	NA	0.399
1	SNP3557	A/a	1	1	NA	0.394
1	SNP4401	A/a	1	1	NA	0.39
1	SNP4870	A/a	1	1	NA	0.395
1	SNP3902	A/a	1	1	NA	0.415
1	SNP2073	A/a	1	1	NA	0.394
1	SNP2376	A/a	1	1	NA	0.399
1	SNP3911	A/a	1	1	NA	0.418
1	SNP4824	A/a	1	1	NA	0.396
1	SNP4767	A/a	1	1	NA	0.386
1	SNP2778	A/a	1	1	NA	0.378
1	SNP4637	A/a	1	1	NA	0.412
1	SNP2679	A/a	1	1	NA	0.382
1	SNP1545	A/a	1	1	NA	0.398
1	SNP4479	A/a	1	1	NA	0.394
1	SNP2225	A/a	1	1	NA	0.393
1	SNP4423	A/a	1	1	NA	0.389
1	SNP3586	A/a	1	1	NA	0.391
1	SNP3070	A/a	1	1	NA	0.381
1	SNP3420	A/a	1	1	NA	0.397
1	SNP2746	A/a	1	1	NA	0.408
1	SNP41	A/a	1	1	NA	0.393
1	SNP1638	A/a	1	1	NA	0.393
1	SNP2548	A/a	1	1	NA	0.409
1	SNP594	A/a	1	1	NA	0.412
1	SNP1538	A/a	1	1	NA	0.413
1	SNP1262	A/a	1	1	NA	0.421
1	SNP1029	A/a	1	1	NA	0.408
1	SNP3808	A/a	1	1	NA	0.405
1	SNP2632	A/a	1	1	NA	0.394
1	SNP3338	A/a	1	1	NA	0.412
1	SNP1853	A/a	1	1	NA	0.421
1	SNP4250	A/a	1	1	NA	0.393
1	SNP889	A/a	1	1	NA	0.414
1	SNP288	A/a	1	1	NA	0.385
1	SNP3073	A/a	1	1	NA	0.39
1	SNP2999	A/a	1	1	NA	0.394
1	SNP2935	A/a	1	1	NA	0.386
1	SNP562	A/a	1	1	NA	0.382
1	SNP2461	A/a	1	1	NA	0.403
1	SNP2954	A/a	1	1	NA	0.399
1	SNP2630	A/a	1	1	NA	0.384
1	SNP1536	A/a	1	1	NA	0.409
