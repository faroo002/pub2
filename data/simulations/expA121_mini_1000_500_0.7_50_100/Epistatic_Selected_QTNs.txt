rep	QTN	snp	allele	chr	pos	cm	maf
1	1	SNP174	A/a	1	1	NA	0.396
1	1	SNP161	A/a	1	1	NA	0.3775
1	2	SNP113	A/a	1	1	NA	0.4195
1	2	SNP115	A/a	1	1	NA	0.3955
1	3	SNP63	A/a	1	1	NA	0.3925
1	3	SNP102	A/a	1	1	NA	0.3915
1	4	SNP441	A/a	1	1	NA	0.413
1	4	SNP31	A/a	1	1	NA	0.3845
1	5	SNP286	A/a	1	1	NA	0.3835
1	5	SNP440	A/a	1	1	NA	0.3815
1	6	SNP220	A/a	1	1	NA	0.396
1	6	SNP132	A/a	1	1	NA	0.3905
1	7	SNP29	A/a	1	1	NA	0.389
1	7	SNP361	A/a	1	1	NA	0.4185
1	8	SNP315	A/a	1	1	NA	0.4035
1	8	SNP269	A/a	1	1	NA	0.395
1	9	SNP165	A/a	1	1	NA	0.4045
1	9	SNP166	A/a	1	1	NA	0.419
1	10	SNP107	A/a	1	1	NA	0.394
1	10	SNP317	A/a	1	1	NA	0.3945
1	11	SNP73	A/a	1	1	NA	0.4105
1	11	SNP124	A/a	1	1	NA	0.406
1	12	SNP429	A/a	1	1	NA	0.406
1	12	SNP478	A/a	1	1	NA	0.4015
1	13	SNP268	A/a	1	1	NA	0.4105
1	13	SNP444	A/a	1	1	NA	0.3845
1	14	SNP288	A/a	1	1	NA	0.412
1	14	SNP463	A/a	1	1	NA	0.411
1	15	SNP377	A/a	1	1	NA	0.3935
1	15	SNP56	A/a	1	1	NA	0.385
1	16	SNP480	A/a	1	1	NA	0.3955
1	16	SNP374	A/a	1	1	NA	0.3915
1	17	SNP49	A/a	1	1	NA	0.396
1	17	SNP8	A/a	1	1	NA	0.4015
1	18	SNP296	A/a	1	1	NA	0.404
1	18	SNP142	A/a	1	1	NA	0.3985
1	19	SNP214	A/a	1	1	NA	0.4035
1	19	SNP321	A/a	1	1	NA	0.3995
1	20	SNP100	A/a	1	1	NA	0.3975
1	20	SNP475	A/a	1	1	NA	0.3995
1	21	SNP138	A/a	1	1	NA	0.3995
1	21	SNP316	A/a	1	1	NA	0.391
1	22	SNP25	A/a	1	1	NA	0.4035
1	22	SNP352	A/a	1	1	NA	0.391
1	23	SNP140	A/a	1	1	NA	0.4345
1	23	SNP344	A/a	1	1	NA	0.404
1	24	SNP224	A/a	1	1	NA	0.405
1	24	SNP239	A/a	1	1	NA	0.4015
1	25	SNP245	A/a	1	1	NA	0.401
1	25	SNP236	A/a	1	1	NA	0.411
