conda env create -f pub2.yml
conda activate h2o4gpu3

conda install -c r r-rcolorbrewer
conda install -c conda-forge r-ggextra 
conda install -c conda-forge r-pheatmap
conda install -c conda-forge r-ggpubr 
conda install -c bioconda r-rgraphics 
conda install -c conda-forge r-stringr 
conda install -c conda-forge r-dplyr 
conda install -c conda-forge r-plyr
conda install -c conda-forge r-backports
conda install -c conda-forge r-devtools
conda install -c r r-randomforest 
conda install -c conda-forge r-ranger 
conda install -c r r-caret 
conda install -c r r-e1071 
conda install -c conda-forge xgboost
conda install -c conda-forge r-bglr 
conda install -c conda-forge r-rrblup 

