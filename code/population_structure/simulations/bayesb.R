#This script will compare models without correcting for population structure. We shall use Arabidopsis RegMap1307 filtered for LD. All markers will be in LE (r<=0.1).
#conda activate h2o4gpu3
########################################################################################
#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
if (length(args)==0) {
  stop("Argument must be supplied.n", call.=FALSE)
} else {
  h2=as.numeric(args[1])
  use_large_effect=as.numeric(args[2])
}
#############################  FUNCTIONS  ##############################################
trim = function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` = purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed = function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################
setwd("./code/population_structure/simulations")
tempdir="./code/population_structure/simulations/temp/"
message("Loading required packages...")
library('dplyr')
library('BEDMatrix')
library('BGLR')
library('rrBLUP')
require('simplePHENOTYPES')	#installed in conda environment py2_env2 and h2o4gpu3

options(scipen = 999)	#To turn off scientific notation
seed=10
set.seed(seed)  #generate same folds sets everytime
###########################################################################################
geno=BEDMatrix('./data/all_1307/call_method_75_TAIR9_maf005_ldpruned',simple_names=TRUE)
geno=as.matrix(geno)
colnames(geno)=as.character(unlist(lapply(strsplit(colnames(geno),"_"),function(arg){arg[1]})))
rownames(geno)=as.character(unlist(lapply(strsplit(rownames(geno),"_"),function(arg){arg[1]})))
MAC=geno-1
geno_sc=scale(geno-1,center=TRUE,scale=TRUE)
G=A.mat(geno-1)
n_snps=ncol(geno)
n_samples=nrow(geno)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
n_folds <- 5
accuracy=data.frame(matrix(nrow=0, ncol=17))
columns=c("n_pca","n_samples","n_snps","n_qtls","cycle","h2","fold","bayesb_nopca_train_r","bayesb_nopca_test_r","bayesb_nopca_train_mse","bayesb_nopca_test_mse","bayesb_pca_train_r","bayesb_pca_test_r","bayesb_pca_train_mse","bayesb_pca_test_mse","causal_snps","epi_snps")
colnames(accuracy)=columns
bayesb_pca=list()
bayesb_nopca=list()
n_cycles=5
pcamodel_index=0
nopcamodel_index=0
H2=h2
h2e=H2-h2
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
for(cycle in c(1:n_cycles))
{
	message(paste0("cycle=",cycle))
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#converting coding as (Minor Allele)aa = -1, Aa = 0 and (Major Allele)AA = 1
	geno[geno==2]=-1
	geno[geno==0]=2
	geno[geno==1]=0
	geno[geno==2]=1
	geno_obj=data.frame(snp=colnames(geno),allele=paste("A/a"),chr=paste("1"),pos=paste("1"),cm=paste("NA"),t(geno))
	rownames(geno_obj)=NULL
	colnames(geno_obj)=c("snp","allele","chr","pos","cm",c(1:n_samples))
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	folds_index <- sample(rep(1:n_folds, length.out = nrow(geno)))
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(n_qtls in c(5,50,100))
	{
		message(paste0("n_qtls=",n_qtls))
		
		whichQTL=sample(colnames(geno),n_qtls,replace=FALSE)
		qtlmatrix=as.matrix(geno[,whichQTL])

		QTN_list <- list()
		QTN_list$add[[1]] <- list(c(whichQTL))
		mu=0
		effect_sizes_dist=rnorm(10000,mu,sqrt(h2))
		big_add_QTN_effect=max(effect_sizes_dist[effect_sizes_dist>(mu+(2*sqrt(h2))) & effect_sizes_dist<=(mu+(3*sqrt(h2)))])	#large effect size will be drawn from the 3 STDEV of normal distribution
		medium_add_QTN_effect=max(effect_sizes_dist[effect_sizes_dist>mu & effect_sizes_dist<=(mu+(1*sqrt(h2)))])

		phenodir=paste0(tempdir,"bayesb_",h2,"_",cycle,"_",n_qtls,"_",use_large_effect)
		system(paste0("mkdir -p ",phenodir))

		if(use_large_effect==1)
		{
			beta=c(big_add_QTN_effect,rep(medium_add_QTN_effect/(n_qtls-1),n_qtls-1))
			add_pheno=create_phenotypes(
								  geno_obj = geno_obj,
								  add_QTN_num = n_qtls,
								  add_effect = list(trait1=beta),					#This will assign effect sizes from geometric series, provided that sim_method != custom
								  sim_method='custom',
								  rep = 1,
								  h2 = h2,
								  model = "A",
								  to_r = TRUE,
								  QTN_list = QTN_list,
								  QTN_variance=TRUE,
								  seed=seed+cycle,
								  output_format='double',
								  home_dir = phenodir,
								  quiet=TRUE
								  )
		}else{
			beta=c(rep(medium_add_QTN_effect/n_qtls,n_qtls))
			add_pheno=create_phenotypes(
								  geno_obj = geno_obj,
								  add_QTN_num = n_qtls,
								  add_effect = list(trait1=beta),					#This will assign effect sizes from geometric series, provided that sim_method != custom
								  sim_method='custom',
								  rep = 1,
								  h2 = h2,
								  model = "A",
								  to_r = TRUE,
								  QTN_list = QTN_list,
								  QTN_variance=TRUE,
								  seed=seed+cycle,
								  output_format='double',
								  home_dir = phenodir,
								  quiet=TRUE
								  )		
		}
		y=add_pheno[,2]
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		for(fold in c(1:n_folds))
		{
			message(paste("fold=",fold))
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			validate=which(folds_index==fold)
			tst=validate
			temp=1:nrow(geno)
			trn=temp[which(temp %not_in% tst)]		
			yNA = y
			yNA[tst] = NA
			train_m=MAC[trn,]
			test_m=MAC[tst,]
			pca <- prcomp(train_m, center = TRUE,scale. = TRUE)
			Wtrn=as.data.frame(pca$x)
			Wtst=as.data.frame(predict(pca,newdata=test_m))
			W=data.frame(matrix(nrow=nrow(MAC),ncol=200))	#saving at max. 200 PCs
			rownames(W)=rownames(MAC)
			colnames(W)=c(paste0("PC",c(1:ncol(W))))
			for(i in c(1:nrow(Wtrn))){ W[which(rownames(W)==rownames(Wtrn[i,])),]=Wtrn[i,]   }
			for(i in c(1:nrow(Wtst))){ W[which(rownames(W)==rownames(Wtst[i,])),]=Wtst[i,]   }
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bayesb_without_pca~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			nopcamodel_index=nopcamodel_index+1
			bayesb_nopca_train_r=-1;bayesb_nopca_train_mse=-1;bayesb_nopca_test_r=-1;bayesb_nopca_test_mse=-1;
			result = tryCatch({
				ETA=list(list(X=geno_sc, model='BayesB'))
				fmA=BGLR(y=yNA,ETA=ETA,thin=5, nIter=6000,burnIn=1000,verbose = FALSE,saveAt=paste0("bayesb_nopca_",n_qtls,"_",h2,"_",use_large_effect))
				bayesb_nopca_train_mse = mean((y[trn]-fmA$yHat[trn])^2)
				bayesb_nopca_train_r = cor(y[trn],fmA$yHat[trn])
				bayesb_nopca_test_mse = mean((y[tst]-fmA$yHat[tst])^2)
				bayesb_nopca_test_r = cor(y[tst],fmA$yHat[tst])
			}, error = function(e){bayesb_nopca_train_r=-1;bayesb_nopca_train_mse=-1;bayesb_nopca_test_r=-1;bayesb_nopca_test_mse=-1;});								
			bayesb_nopca[[nopcamodel_index]]<-fmA
			for(n_pca in c(2,3,4,5,6,7,8,9,10,20,50))
			{
				#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bayesb_with_pca~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				pcamodel_index=pcamodel_index+1
				bayesb_pca_train_r=-1;bayesb_pca_train_mse=-1;bayesb_pca_test_r=-1;bayesb_pca_test_mse=-1;
				result = tryCatch({
					ETA=list(list(X=W[,c(1:n_pca)], model="FIXED"),list(X=geno_sc, model='BayesB'))
					fmA=BGLR(y=yNA,ETA=ETA,thin=5, nIter=6000,burnIn=1000,verbose = FALSE,saveAt=paste0("bayesb_pca_",n_qtls,"_",h2,"_",use_large_effect))
					bayesb_pca_train_mse = mean((y[trn]-fmA$yHat[trn])^2)
					bayesb_pca_train_r = cor(y[trn],fmA$yHat[trn])
					bayesb_pca_test_mse = mean((y[tst]-fmA$yHat[tst])^2)
					bayesb_pca_test_r = cor(y[tst],fmA$yHat[tst])
				}, error = function(e){bayesb_pca_train_r=-1;bayesb_pca_train_mse=-1;bayesb_pca_test_r=-1;bayesb_pca_test_mse=-1;});								
				bayesb_pca[[pcamodel_index]]<-fmA
				#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				causal_snps=paste0(whichQTL,collapse=",")
				epi_snps= ""
				df=data.frame(matrix(c(n_pca,n_samples,n_snps,n_qtls,cycle,h2,fold,bayesb_nopca_train_r,bayesb_nopca_test_r,bayesb_nopca_train_mse,bayesb_nopca_test_mse,bayesb_pca_train_r,bayesb_pca_test_r,bayesb_pca_train_mse,bayesb_pca_test_mse,causal_snps,epi_snps),nrow=1))
				colnames(df)=columns
				accuracy=rbind(accuracy,df)
				print(tail(accuracy[,-c(16,17)],1))
				#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			}#n_pca
		}#folds
	}
	saveRDS(accuracy,paste0("bayesb_accuracy","_",h2,"_",use_large_effect,"3.rds"))
	saveRDS(bayesb_nopca,paste0("bayesb_nopca_models","_",h2,"_",use_large_effect,"3.rds"))
	saveRDS(bayesb_pca,paste0("bayesb_pca_models","_",h2,"_",use_large_effect,"3.rds"))
}#cycles

message("Job completed!!!")
###########################################################################################
