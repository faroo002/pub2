#This script will compare XGB models with and without correcting for population structure. 
#This script will compare models without correcting for population structure. We shall use Arabidopsis Baxter's dataset filtered for LD @0.9.
#conda activate h2o4gpu3
#############################  FUNCTIONS  ##############################################
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` <- purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed <- function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################
setwd("./code/population_structure/real")
message("Loading required packages...")
require('dplyr')
require('simplePHENOTYPES')
require('h2o4gpu')
require('BEDMatrix')
#require('ggpubr')
library(rrBLUP)
#require(RColorBrewer)
library(foreign); 
library(nnet);
options(scipen = 999)	#To turn off scientific notation
seed=10
set.seed(seed)  #generate same folds sets everytime
###########################################################################################
###########################################################################################
###########################################################################################
###########################################################################################
geno=BEDMatrix('./data/baxter/call_method_75_TAIR9_maf005_ldpruned',simple_names=TRUE)
geno=as.matrix(geno)
colnames(geno)=as.character(unlist(lapply(strsplit(colnames(geno),"_"),function(arg){arg[1]})))
rownames(geno)=as.character(unlist(lapply(strsplit(rownames(geno),"_"),function(arg){arg[1]})))
n_snps=ncol(geno)
n_samples=nrow(geno)

pheno=read.table("pheno.tbl",sep="\t",header=T)
pheno$Unique_ID=factor(pheno$Unique_ID)
marker_names=colnames(geno)
accessions=rownames(geno)
geno=data.frame(factor(accessions),geno)
colnames(geno)=c("Unique_ID",marker_names)
geno_pheno<-left_join(geno,pheno,by="Unique_ID")
geno_df<-geno_pheno[,1:ncol(geno)]
geno_df<-geno_df[,-1] #remove accessions column
rownames(geno_df) <- accessions
s<-ncol(geno) + 1
pheno_df<-as.data.frame(geno_pheno[,s:ncol(geno_pheno)])
rownames(pheno_df) <- accessions
geno_sc=scale(geno_df-1,center=TRUE,scale=TRUE)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
MAC=geno_df
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
n_folds <- 5
accuracy=data.frame(matrix(nrow=0, ncol=14))
columns=c("n_pca","n_samples","n_snps","cycle","fold","xgb_train_r","xgb_test_r","xgb_train_mse","xgb_test_mse","xgb_adj_train_r","xgb_adj_test_r","xgb_adj_train_mse","xgb_adj_test_mse","imp")
colnames(accuracy)=columns
xgb_pca=list()
xgb_nopca=list()
n_cycles=5
nopcamodel_index=0
pcamodel_index=0
y=pheno_df[,'Na_level']
covariates=pheno_df[,c('toSeaSal','toSea','toSal')]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
for(cycle in c(1:n_cycles))
{
	message(paste0("cycle=",cycle))
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	folds_index <- sample(rep(1:n_folds, length.out = nrow(geno)))
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(fold in c(1:n_folds))
	{
		message(paste("fold:",fold))
		validate=which(folds_index==fold)
		tst=validate
		temp=1:nrow(MAC)
		trn=temp[which(temp %not_in% tst)]		
		train_m=MAC[trn,]
		test_m=MAC[tst,]
		train_y=y[trn]
		test_y=y[tst]
		pca <- prcomp(train_m, center = TRUE,scale. = TRUE)
		Wtrn=as.data.frame(pca$x)
		Wtst=as.data.frame(predict(pca,newdata=test_m))
		W=data.frame(matrix(nrow=nrow(MAC),ncol=200))	#saving at max. 200 PCs
		rownames(W)=rownames(MAC)
		colnames(W)=c(paste0("PC",c(1:ncol(W))))
		for(i in c(1:nrow(Wtrn))){ W[which(rownames(W)==rownames(Wtrn[i,])),]=Wtrn[i,]   }
		for(i in c(1:nrow(Wtst))){ W[which(rownames(W)==rownames(Wtst[i,])),]=Wtst[i,]   }
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TUNING~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		n_grid_folds=5
		n_train_samples=n_samples-round(n_samples/n_folds)
		n_grid_train_samples=n_train_samples-round(n_train_samples/n_grid_folds)
		grid_folds_index <- sample(rep(1:n_grid_folds, length.out = nrow(train_m)))
		
		tuning=data.frame(matrix(nrow=0, ncol=7))
		columns=c("max_depth","colsample_bytree","subsample","train_r","test_r","train_mse","test_mse")
		colnames(tuning)=columns
		
		for(max_depth in c(4,10,50,100))
		{
			for(colsample_bytree in c(0.3,0.5,0.7))
			{
				for(subsample in c(0.7,0.9))
				{
					message(paste0("max_depth:",max_depth," colsample_bytree:",colsample_bytree," subsample:",subsample))
					grid_folds_overall_outcome=data.frame(matrix(nrow=0, ncol=5))
					columns=c("grid_fold","grid_train_r","grid_test_r","grid_train_mse","grid_test_mse")
					colnames(grid_folds_overall_outcome)=columns
					for(grid_fold in c(1:n_grid_folds))
					{
						grid_validate=which(grid_folds_index==grid_fold)
						grid_tst=grid_validate
						temp=1:nrow(train_m)
						grid_trn=temp[which(temp %not_in% grid_tst)]		
						grid_test_m=train_m[grid_tst,]
						grid_test_y=train_y[grid_tst]
						grid_train_m=train_m[-grid_tst,]
						grid_train_y=train_y[-grid_tst]
						model <- h2o4gpu.gradient_boosting_regressor(
									max_depth=max_depth,
									random_state=10L,
									n_jobs=100L,
									verbose=1,
									colsample_bytree=colsample_bytree,
									subsample=subsample,predictor='cpu_predictor',tree_method='auto'
									) %>% fit(grid_train_m,grid_train_y) 
						grid_train_y_pred=model %>% predict(grid_train_m)
						grid_train_r=cor(grid_train_y_pred,grid_train_y)
						grid_test_y_pred=model %>% predict(grid_test_m)
						grid_test_r=cor(grid_test_y_pred,grid_test_y)
						grid_train_mse = mean((grid_train_y-grid_train_y_pred)^2)
						grid_test_mse = mean((grid_test_y-grid_test_y_pred)^2)						
						df=data.frame(matrix(c(grid_fold,grid_train_r,grid_test_r,grid_train_mse,grid_test_mse),nrow=1))
						colnames(df)=columns
						grid_folds_overall_outcome=rbind(grid_folds_overall_outcome,df)
					}
					df=data.frame(matrix(c(max_depth,colsample_bytree,subsample,mean(grid_folds_overall_outcome$grid_train_r),mean(grid_folds_overall_outcome$grid_test_r),mean(grid_folds_overall_outcome$grid_train_mse),mean(grid_folds_overall_outcome$grid_test_mse)),nrow=1))
					columns=c("max_depth","colsample_bytree","subsample","train_r","test_r","train_mse","test_mse")
					colnames(df)=columns
					tuning=rbind(tuning,df)
				}
			}
		}

		#To fetch the best combination of parameters
		best_params=data.frame(matrix(nrow=0, ncol=3))
		columns=c("max_depth","colsample_bytree","subsample")
		colnames(best_params)=columns
		df=tuning[which(tuning$test_r==max(tuning$test_r)),]
		best_params=rbind(best_params,df[1,c("max_depth","colsample_bytree","subsample")])
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~xgb~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		nopcamodel_index=nopcamodel_index+1
		model <- h2o4gpu.gradient_boosting_regressor(
					max_depth=best_params$max_depth,
					random_state=10L,
					n_jobs=100L,
					verbose=1,
					colsample_bytree=best_params$colsample_bytree,
					subsample=best_params$subsample,predictor='cpu_predictor',tree_method='auto'
					) %>% fit(train_m,train_y) 
		train_y_pred=model %>% predict(train_m)
		train_r=cor(train_y_pred,train_y)
		test_y_pred=model %>% predict(test_m)
		test_r=cor(test_y_pred,test_y)
		train_mse = mean((train_y-train_y_pred)^2)
		test_mse = mean((test_y-test_y_pred)^2)
		xgb_nopca[[nopcamodel_index]]<-model
		
		for(n_pca in c(2,3,4,5,6,7,8,9,10,20,50))
		{
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			df_train_m=data.frame(W[trn,1:n_pca],train_m)
			colnames(df_train_m)=c(paste0("PC",c(1:n_pca)),paste0("m",c(1:ncol(train_m))))
			df_test_m=data.frame(W[tst,1:n_pca],test_m)
			colnames(df_test_m)=c(paste0("PC",c(1:n_pca)),paste0("m",c(1:ncol(test_m))))
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			pcamodel_index=pcamodel_index+1
			
			model_pca <- h2o4gpu.gradient_boosting_regressor(
					max_depth=best_params$max_depth,
					random_state=10L,
					n_jobs=100L,
					verbose=1,
					colsample_bytree=best_params$colsample_bytree,
					subsample=best_params$subsample,predictor='cpu_predictor',tree_method='auto'
					) %>% fit(df_train_m,train_y) 
			train_y_pred_adj=model_pca %>% predict(df_train_m)
			train_r_adj=cor(train_y_pred_adj,train_y)
			test_y_pred_adj=model_pca %>% predict(df_test_m)
			test_r_adj=cor(test_y_pred_adj,test_y)
			train_mse_adj = mean((train_y-train_y_pred_adj)^2)
			test_mse_adj = mean((test_y-test_y_pred_adj)^2)
			xgb_pca[[pcamodel_index]]<-model_pca
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~		
			imp=paste0(model$feature_importances_,collapse=',')
			df=data.frame(matrix(c(n_pca,n_samples,n_snps,cycle,fold,train_r,test_r,train_mse,test_mse,train_r_adj,test_r_adj,train_mse_adj,test_mse_adj,imp),nrow=1))
			columns=c("n_pca","n_samples","n_snps","cycle","fold","xgb_train_r","xgb_test_r","xgb_train_mse","xgb_test_mse","xgb_adj_train_r","xgb_adj_test_r","xgb_adj_train_mse","xgb_adj_test_mse","imp")
			colnames(df)=columns
			accuracy=rbind(accuracy,df)
			print(tail(accuracy[,c(-14)],1))
			
		}
	}#folds
	saveRDS(accuracy,paste0("xgb_accuracy2.rds"))
	saveRDS(xgb_nopca,paste0("xgb_nopca_models2.rds"))
	saveRDS(xgb_pca,paste0("xgb_pca_models2.rds"))

}#cycles

message("Job completed!!!")
