########################################################################################
#This script will compare GBLUP variances calculated using BGLR with ASREML outcomes to reproduce some results of the Norman et al. 2017
#using BLUPs as phenotypes for each genotype.
########################################################################################
#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
n_train_samples=500
n_train_rep=1
if (length(args)==0) {
  stop("Argument must be supplied.n", call.=FALSE)
} else {
  trait=as.character(args[1])
}
#############################  FUNCTIONS  ##############################################
trim = function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` = purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed = function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
}
########################################################################################
setwd("/mnt/LTR_userdata/faroo002/arabidopsis/GP/using_conda/ML/real_geno_real_pheno/wheat/trait_complexity/gblup")
message("Loading required packages...")
library('dplyr')
library('BGLR')
set.seed(10)  #generate same folds sets everytime
###########################################################################################
geno=readRDS("/mnt/LTR_userdata/faroo002/wheat/norman/wheat_geno.rds")
n_snps=ncol(geno)
n_samples=nrow(geno)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
M=as.matrix(geno)
SS <- tcrossprod(M) 
N <- sum(diag(SS))/n_snps
G <- SS / N
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
pheno=read.table("/mnt/LTR_userdata/faroo002/arabidopsis/GP/using_conda/ML/real_geno_real_pheno/wheat/pheno_blups.tbl",sep="\t",header=T)	
marker_names=colnames(geno)
accessions=rownames(geno)
geno=data.frame(accessions,geno)
colnames(geno)=c("Genotype",marker_names)
geno_pheno<-left_join(geno,pheno,by="Genotype")
geno_df<-geno_pheno[,1:ncol(geno)]
geno_df<-geno_df[,-1] #remove accessions column
rownames(geno_df) <- accessions
s<-ncol(geno) + 1
pheno_df<-as.data.frame(geno_pheno[,s:ncol(geno_pheno)])
rownames(pheno_df) <- accessions
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gblup_G=list()
gblup_Ga=list()
y = pheno_df[,trait]
model_index=0
vars=apply(geno_df,2,var)
geno_df=geno_df[,which(vars>0)]
geno_sc=scale(geno_df,center=T,scale=T)
yNA = y
n_snps=ncol(geno_df)
n_samples=nrow(geno_df)

model_index=model_index+1		
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~gblup_G~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gblup_Gr=-1;
result = tryCatch({
	ETA=list(list(K=G, model='RKHS'),list(K=diag(1,nrow=nrow(G),ncol=ncol(G)), model='RKHS'))
	fmA=BGLR(y=yNA,ETA=ETA,thin=5, nIter=6000,burnIn=1000,verbose = FALSE,saveAt="gblup_G_")
	gblup_G[[model_index]]<-fmA
}, error = function(e){gblup_Gr=-1;gblup_G[[model_index]]<-list()});
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~gblup_Ga~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gblup_Ga_r=-1;
result = tryCatch({
	ETA=list(list(K=G, model='RKHS'))
	fmA=BGLR(y=yNA,ETA=ETA,thin=5, nIter=6000,burnIn=1000,verbose = FALSE,saveAt="gblup_Ga_")
	gblup_Ga[[model_index]]<-fmA
}, error = function(e){gblup_Ga_r=-1;gblup_Ga[[model_index]]<-list()});		
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
saveRDS(gblup_G,paste0(trait,"_gblup_full.rds"))
saveRDS(gblup_Ga,paste0(trait,"_gblup_Ga.rds"))
##################################################################################################################################
