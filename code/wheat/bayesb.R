#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
n_train_samples=8300
n_train_rep=1
if (length(args)==0) {
  stop("Argument must be supplied.n", call.=FALSE)
} else {
  trait=as.character(args[1])
  n_train_samples=as.numeric(args[2])
  n_train_rep=as.numeric(args[3])  
}
#############################  FUNCTIONS  ##############################################
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` <- purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed <- function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################
setwd("./code/wheat")
message("Loading required packages...")
require('dplyr')
#require(randomForest)
#require(ranger)
#require(caret)
#require(e1071)
#require('qgg')
#require('xgboost')
#require('BEDMatrix')
require('ggpubr')
#require('xgboost')
#require(RColorBrewer)
require('BGLR')
require('rrBLUP')
options(scipen = 999)	#To turn off scientific notation
set.seed(10)  #generate same folds sets everytime
###########################################################################################
###########################################################################################
#				>>BayesB
###########################################################################################
###########################################################################################
n_folds <- 5
accuracy=data.frame(matrix(nrow=0, ncol=9))
columns=c("trait","n_train_samples","n_train_rep","cycle","fold","bayesb_train_r","bayesb_test_r","bayesb_train_mse","bayesb_test_mse")
colnames(accuracy)=columns
bayesb=list()
n_cycles=1
model_index=0
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
geno=readRDS("./data/wheat/wheat_geno.rds")
n_snps=ncol(geno)
n_samples=nrow(geno)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
pheno=read.table("./data/wheat",sep="\t",header=T)	
marker_names=colnames(geno)
accessions=rownames(geno)
geno=data.frame(accessions,geno)
colnames(geno)=c("Genotype",marker_names)
geno_pheno<-left_join(geno,pheno,by="Genotype")
geno_df<-geno_pheno[,1:ncol(geno)]
geno_df<-geno_df[,-1] #remove accessions column
rownames(geno_df) <- accessions
s<-ncol(geno) + 1
pheno_df<-as.data.frame(geno_pheno[,s:ncol(geno_pheno)])
rownames(pheno_df) <- accessions
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
y=pheno_df[,trait]
geno_var=apply(geno_df,2,var)
geno_df=geno_df[,-c(which(geno_var==0))] 
geno_sc=scale(as.matrix(geno_df))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
for(cycle in c(1:n_cycles))
{
	folds_index <- sample(rep(1:n_folds, length.out = nrow(geno_df)))
	validate=matrix(nrow=round(nrow(geno_df)/n_folds),ncol=n_folds)
	for(i in c(1:n_folds))
	{
		validate[,i]=which(folds_index==i)
	}
	#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(fold in c(1:n_folds))
	{
		message(paste("fold:",fold))
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		tst=validate[,fold]
		temp=1:nrow(geno_df)
		trn=temp[which(temp %not_in% tst)]
		if(length(trn)>n_train_samples)
        {
            trn=sample(trn,n_train_samples,replace=FALSE)
        }		
		yNA = y
		yNA[tst] = NA
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bayesb_all~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		model_index=model_index+1
		bayesb_train_r=-1;bayesb_train_mse=-1;bayesb_test_r=-1;bayesb_test_mse=-1;
		result = tryCatch({
			ETA=list(list(X=geno_sc, model='BayesB',saveEffects=TRUE))
			fmA=BGLR(y=yNA,ETA=ETA,thin=5, nIter=6000,burnIn=1000,verbose = FALSE,saveAt=paste0("bayesb_",n_train_samples,"_",n_train_rep,"_",trait,"_"))
			bayesb_train_mse = mean((y[trn]-fmA$yHat[trn])^2)
			bayesb_train_r = cor(y[trn],fmA$yHat[trn])
			bayesb_test_mse = mean((y[tst]-fmA$yHat[tst])^2)
			bayesb_test_r = cor(y[tst],fmA$yHat[tst])
		}, error = function(e){bayesb_train_r=-1;bayesb_train_mse=-1;bayesb_test_r=-1;bayesb_test_mse=-1;});								
		bayesb[[model_index]]<-fmA
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		df=data.frame(matrix(c(trait,n_train_samples,n_train_rep,cycle,fold,bayesb_train_r,bayesb_test_r,bayesb_train_mse,bayesb_test_mse),nrow=1))
		colnames(df)=columns
		accuracy=rbind(accuracy,df)
		print(tail(accuracy,1))
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	}#folds
	saveRDS(accuracy,paste0("wheat_bayesb_accuracy_",n_train_samples,"_",n_train_rep,"_",trait,".rds"))
	saveRDS(bayesb,paste0("wheat_bayesb_models_",n_train_samples,"_",n_train_rep,"_",trait,".rds"))
}#cycles

message("Job completed!!!")
