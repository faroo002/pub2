########################################################################################
#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
if (length(args)==0) {
  stop("Argument must be supplied.n", call.=FALSE)
} else {
  n_samples=as.numeric(args[1])
  n_snps=as.numeric(args[2])
  h2=as.numeric(args[3])
  use_large_effect=as.numeric(args[4])
}
#############################  FUNCTIONS  ##############################################
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` <- purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed <- function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################
setwd("./code/simulations")
tempdir="./code/simulations/temp/"
message("Loading required packages...")
require('simplePHENOTYPES')
require('dplyr')
#require(randomForest)
#require(ranger)
#require(caret)
#require(e1071)
#require('qgg')
#require('xgboost')
#require('BEDMatrix')
#require('ggpubr')
#require('xgboost')
#require(RColorBrewer)
require('BGLR')
require('rrBLUP')
options(scipen = 999)	#To turn off scientific notation
seed=10
set.seed(seed)  #generate same folds sets everytime
###########################################################################################
###########################################################################################
#	1.	Using foldulated genotypes and foldulated phenotypes
#		A.	Experiment-A: Effect of GP Space Dimensionality (sample/markers sizes)
#			a)	Experiment-A1.2: Non-Linear Phenotypes (Additive + Epistatic effects)
#				>>bayesa
###########################################################################################
###########################################################################################
n_folds <- 5
accuracy=data.frame(matrix(nrow=0, ncol=12))
columns=c("n_samples","n_snps","n_qtls","cycle","h2","fold","bayesa_train_r","bayesa_test_r","bayesa_train_mse","bayesa_test_mse","causal_snps","epi_snps")
colnames(accuracy)=columns
bayesa=list()
n_cycles=5
model_index=0
H2=0.8
h2e=H2-h2
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(cycle in c(1:n_cycles))
	{
		message(paste0("cycle=",cycle))
		maf = rep(0.4,n_snps)
		geno = sapply(maf,rbinom,n=n_samples,size=2)	#here 2 is coded as minor allele (not the second allele term)
		colnames(geno)=paste0("SNP",c(1:n_snps))
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		#converting coding as (Minor Allele)aa = -1, Aa = 0 and (Major Allele)AA = 1
		geno[geno==2]=-1
		geno[geno==0]=2
		geno[geno==1]=0
		geno[geno==2]=1
		geno_obj=data.frame(snp=colnames(geno),allele=paste("A/a"),chr=paste("1"),pos=paste("1"),cm=paste("NA"),t(geno))
		rownames(geno_obj)=NULL
		colnames(geno_obj)=c("snp","allele","chr","pos","cm",c(1:n_samples))
		geno_sc=scale(geno,scale=TRUE,center=TRUE)
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		folds_index <- sample(rep(1:n_folds, length.out = nrow(geno)))
		validate=matrix(nrow=round(nrow(geno)/n_folds),ncol=n_folds)
		for(i in c(1:n_folds))
		{
			validate[,i]=which(folds_index==i)
		}
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		for(n_qtls in c(5,50,100))
		{
			message(paste0("n_qtls=",n_qtls))
			
		whichQTL=sapply(sample(1:n_snps,n_qtls,replace=FALSE),function(x){paste0("SNP",x)})
		qtlmatrix=as.matrix(geno[,whichQTL])
		epiQTL=sample(whichQTL,2,replace=FALSE)

		QTN_list <- list()
		QTN_list$add[[1]] <- list(c(whichQTL))
		QTN_list$epi[[1]] <- c(epiQTL)
		mu=0
		effect_sizes_dist=rnorm(10000,mu,sqrt(h2))
		big_add_QTN_effect=max(effect_sizes_dist[effect_sizes_dist>(mu+(2*sqrt(h2))) & effect_sizes_dist<=(mu+(3*sqrt(h2)))])	#large effect size will be drawn from the 3 STDEV of normal distribution
		medium_add_QTN_effect=max(effect_sizes_dist[effect_sizes_dist>mu & effect_sizes_dist<=(mu+(1*sqrt(h2)))])
		epi_effect_sizes_dist=rnorm(10000,mu,sqrt(h2e))
		epi_QTN_effect=max(epi_effect_sizes_dist[epi_effect_sizes_dist>mu & epi_effect_sizes_dist<=(mu+(1*sqrt(h2e)))])


		phenodir=paste0(tempdir,"expA124_",n_samples,"_",n_snps,"_",h2,"_",cycle,"_",use_large_effect)
		system(paste0("mkdir -p ",phenodir))

		if(use_large_effect==1)
		{
			beta=c(big_add_QTN_effect,rep(medium_add_QTN_effect/(n_qtls-1),n_qtls-1))
			epi_pheno=create_phenotypes(
								  geno_obj = geno_obj,
								  add_QTN_num = n_qtls,
								  add_effect = list(trait1=beta),					#This will assign effect sizes from geometric series, provided that sim_method != custom
								  sim_method='custom',
								  rep = 1,
								  h2 = h2,
								  model = "AE",
								  to_r = TRUE,
								  epi_QTN_num = length(epiQTL)-1,
								  epi_interaction = 2,
								  epi_effect = epi_QTN_effect,
								  QTN_list = QTN_list,
								  QTN_variance=TRUE,
								  seed=seed+cycle,
								  output_format='double',
								  home_dir = phenodir,
								  quiet=TRUE
								  )
		}else{
			beta=c(rep(medium_add_QTN_effect/n_qtls,n_qtls))
			epi_pheno=create_phenotypes(
								  geno_obj = geno_obj,
								  add_QTN_num = n_qtls,
								  add_effect = list(trait1=beta),					#This will assign effect sizes from geometric series, provided that sim_method != custom
								  sim_method='custom',
								  rep = 1,
								  h2 = h2,
								  model = "AE",
								  to_r = TRUE,
								  epi_QTN_num = length(epiQTL)-1,
								  epi_interaction = 2,
								  epi_effect = epi_QTN_effect,
								  QTN_list = QTN_list,
								  QTN_variance=TRUE,
								  seed=seed+cycle,
								  output_format='double',
								  home_dir = phenodir,
								  quiet=TRUE
								  )		
		}
		y=epi_pheno[,2]
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		for(fold in c(1:n_folds))
		{
			message(paste("fold=",fold))
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			tst=validate[,fold]
			temp=1:nrow(geno)
			trn=temp[which(temp %not_in% tst)]		
			yNA = y
			yNA[tst] = NA
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bayesa_all~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			model_index=model_index+1
			bayesa_train_r=-1;bayesa_train_mse=-1;bayesa_test_r=-1;bayesa_test_mse=-1;
			result = tryCatch({
				ETA=list(list(X=geno_sc, model='BayesA'))
				fmA=BGLR(y=yNA,ETA=ETA,thin=5, nIter=6000,burnIn=1000,verbose = FALSE,saveAt=paste0(n_samples,"_",n_snps,"_",h2,"_",use_large_effect))
				bayesa_train_mse = mean((y[trn]-fmA$yHat[trn])^2)
				bayesa_train_r = cor(y[trn],fmA$yHat[trn])
				bayesa_test_mse = mean((y[tst]-fmA$yHat[tst])^2)
				bayesa_test_r = cor(y[tst],fmA$yHat[tst])
			}, error = function(e){bayesa_train_r=-1;bayesa_train_mse=-1;bayesa_test_r=-1;bayesa_test_mse=-1;});								
			bayesa[[model_index]]<-fmA
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			causal_snps=paste0(whichQTL,collapse=",")
			epi_snps= paste0(epiQTL,collapse=",")
			df=data.frame(matrix(c(n_samples,n_snps,n_qtls,cycle,h2,fold,bayesa_train_r,bayesa_test_r,bayesa_train_mse,bayesa_test_mse,causal_snps,epi_snps),nrow=1))
			colnames(df)=columns
			accuracy=rbind(accuracy,df)
			print(tail(accuracy[,c(-11,-12)],1))
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		}#folds
	}
}#cycles

saveRDS(accuracy,paste0("expA124_bayesa_accuracy_",n_samples,"_",n_snps,"_",h2,"_",use_large_effect,".rds"))
saveRDS(bayesa,paste0("expA124_bayesa_",n_samples,"_",n_snps,"_",h2,"_",use_large_effect,".rds"))
message("Job completed!!!")
