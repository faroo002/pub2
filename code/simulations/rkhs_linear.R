########################################################################################
#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
if (length(args)==0) {
  stop("Argument must be supplied.n", call.=FALSE)
} else {
  n_samples=as.numeric(args[1])
  n_snps=as.numeric(args[2])
  h2=as.numeric(args[3])
  use_large_effect=as.numeric(args[4])
}
#############################  FUNCTIONS  ##############################################
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
`%not_in%` <- purrr::negate(`%in%`)
# Function to check whether package is installed
is.installed <- function(mypkg)
{
    is.element(mypkg, installed.packages()[,1])
} 
########################################################################################
setwd("./code/simulations")
tempdir="./code/simulations/temp/"
message("Loading required packages...")
require('simplePHENOTYPES')	#installed in conda environment py2_env2
require('dplyr')
#require(randomForest)
#require(ranger)
#require(caret)
#require(e1071)
#require('qgg')
#require('xgboost')
#require('BEDMatrix')
#require('ggpubr')
#require('xgboost')
#require(RColorBrewer)
require('BGLR')
require('rrBLUP')
options(scipen = 999)	#To turn off scientific notation
seed=10
set.seed(seed)  #generate same folds sets everytime
###########################################################################################
###########################################################################################
#	1.	Using foldulated genotypes and foldulated phenotypes
#		A.	Experiment-A: Effect of GP Space Dimensionality (sample/markers sizes)
#			a)	Experiment-A1.2: Non-Linear Phenotypes (Additive + Epistatic effects)
#				>>rkhs
###########################################################################################
###########################################################################################
n_folds <- 5
accuracy=data.frame(matrix(nrow=0, ncol=12))
columns=c("n_samples","n_snps","n_qtls","cycle","h2","fold","rkhs_train_r","rkhs_test_r","rkhs_train_mse","rkhs_test_mse","causal_snps","epi_snps")
colnames(accuracy)=columns
rkhs=list()
n_cycles=5
model_index=0
H2=h2
h2e=H2-h2
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(cycle in c(1:n_cycles))
	{
		message(paste0("cycle=",cycle))
		maf = rep(0.4,n_snps)
		geno = sapply(maf,rbinom,n=n_samples,size=2)	#here 2 is coded as minor allele (not the second allele term)
		colnames(geno)=paste0("SNP",c(1:n_snps))

		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		#converting coding as (Minor Allele)aa = -1, Aa = 0 and (Major Allele)AA = 1
		geno[geno==2]=-1
		geno[geno==0]=2
		geno[geno==1]=0
		geno[geno==2]=1
		geno_obj=data.frame(snp=colnames(geno),allele=paste("A/a"),chr=paste("1"),pos=paste("1"),cm=paste("NA"),t(geno))
		rownames(geno_obj)=NULL
		colnames(geno_obj)=c("snp","allele","chr","pos","cm",c(1:n_samples))
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		X = scale(geno, center=TRUE, scale=TRUE)        #This is another way of centering and scaling because STDEV=sqrt(2*p*(1-p)) and mean=2*p
        RD<-(as.matrix(dist(X,method='euclidean'))^2)/ncol(geno)
        h= c(0.2,0.5,0.8)  #this is bandwidth parameter i.e. it controls the variance of the guassian distribution for constructing GRM. It can be calculated using bayesian method or cross validation but here I am setting it arbitrary first. It can be smaller for self population as mentioned in http://genomics.cimmyt.org/BGLR-extdoc.pdf
		RK1=exp(-h[1]*RD)
		RK2=exp(-h[2]*RD)
		RK3=exp(-h[3]*RD)
		geno_sc=scale(geno,scale=TRUE,center=TRUE)
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		folds_index <- sample(rep(1:n_folds, length.out = nrow(geno)))
		validate=matrix(nrow=round(nrow(geno)/n_folds),ncol=n_folds)
		for(i in c(1:n_folds))
		{
			validate[,i]=which(folds_index==i)
		}
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		for(n_qtls in c(5,50,100))
		{
			message(paste0("n_qtls=",n_qtls))
			
		whichQTL=sapply(sample(1:n_snps,n_qtls,replace=FALSE),function(x){paste0("SNP",x)})
		qtlmatrix=as.matrix(geno[,whichQTL])

		QTN_list <- list()
		QTN_list$add[[1]] <- list(c(whichQTL))
		mu=0
		effect_sizes_dist=rnorm(10000,mu,sqrt(h2))
		big_add_QTN_effect=max(effect_sizes_dist[effect_sizes_dist>(mu+(2*sqrt(h2))) & effect_sizes_dist<=(mu+(3*sqrt(h2)))])	#large effect size will be drawn from the 3 STDEV of normal distribution
		medium_add_QTN_effect=max(effect_sizes_dist[effect_sizes_dist>mu & effect_sizes_dist<=(mu+(1*sqrt(h2)))])

		phenodir=paste0(tempdir,"expA113_",n_samples,"_",n_snps,"_",h2,"_",cycle,"_",use_large_effect)
		system(paste0("mkdir -p ",phenodir))

		if(use_large_effect==1)
		{
			beta=c(big_add_QTN_effect,rep(medium_add_QTN_effect/(n_qtls-1),n_qtls-1))
			add_pheno=create_phenotypes(
								  geno_obj = geno_obj,
								  add_QTN_num = n_qtls,
								  add_effect = list(trait1=beta),					#This will assign effect sizes from geometric series, provided that sim_method != custom
								  sim_method='custom',
								  rep = 1,
								  h2 = h2,
								  model = "A",
								  to_r = TRUE,
								  QTN_list = QTN_list,
								  QTN_variance=TRUE,
								  seed=seed+cycle,
								  output_format='double',
								  home_dir = phenodir,
								  quiet=TRUE
								  )
		}
		else
		{
			beta=c(rep(medium_add_QTN_effect/n_qtls,n_qtls))
			add_pheno=create_phenotypes(
								  geno_obj = geno_obj,
								  add_QTN_num = n_qtls,
								  add_effect = list(trait1=beta),					#This will assign effect sizes from geometric series, provided that sim_method != custom
								  sim_method='custom',
								  rep = 1,
								  h2 = h2,
								  model = "A",
								  to_r = TRUE,
								  QTN_list = QTN_list,
								  QTN_variance=TRUE,
								  seed=seed+cycle,
								  output_format='double',
								  home_dir = phenodir,
								  quiet=TRUE
								  )		
		}
		y=add_pheno[,2]
		#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		for(fold in c(1:n_folds))
		{
			message(paste("fold=",fold))
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			tst=validate[,fold]
			temp=1:nrow(geno)
			trn=temp[which(temp %not_in% tst)]		
			yNA = y
			yNA[tst] = NA
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~rkhs_all~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			model_index=model_index+1
			rkhs_train_r=-1;rkhs_train_mse=-1;rkhs_test_r=-1;rkhs_test_mse=-1;
			result = tryCatch({
				ETA=list(list(K=RK1, model='RKHS'),list(K=RK2, model='RKHS'),list(K=RK3, model='RKHS'))
				fmA=BGLR(y=yNA,ETA=ETA,thin=5, nIter=6000,burnIn=1000,verbose = FALSE,saveAt=paste0(n_samples,"_",n_snps,"_",h2,"_",use_large_effect))
				rkhs_train_mse = mean((y[trn]-fmA$yHat[trn])^2)
				rkhs_train_r = cor(y[trn],fmA$yHat[trn])
				rkhs_test_mse = mean((y[tst]-fmA$yHat[tst])^2)
				rkhs_test_r = cor(y[tst],fmA$yHat[tst])
			}, error = function(e){rkhs_train_r=-1;rkhs_train_mse=-1;rkhs_test_r=-1;rkhs_test_mse=-1;});								
			rkhs[[model_index]]<-fmA
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			causal_snps=paste0(whichQTL,collapse=",")
			epi_snps= ""
			df=data.frame(matrix(c(n_samples,n_snps,n_qtls,cycle,h2,fold,rkhs_train_r,rkhs_test_r,rkhs_train_mse,rkhs_test_mse,causal_snps,epi_snps),nrow=1))
			colnames(df)=columns
			accuracy=rbind(accuracy,df)
			print(tail(accuracy[,c(-11,-12)],1))
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		}#folds
	}
}#cycles

saveRDS(accuracy,paste0("expA113_rkhs_accuracy_",n_samples,"_",n_snps,"_",h2,"_",use_large_effect,".rds"))
saveRDS(rkhs,paste0("expA113_rkhs_",n_samples,"_",n_snps,"_",h2,"_",use_large_effect,".rds"))
message("Job completed!!!")
